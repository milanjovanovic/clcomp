(in-package #:clcomp)

(defun print-byte (n)
  (format t "#b~8,'0b" n)
  (format t "~%#x~2,'0x" n)
  (format t "~%~D" n))

(defparameter *byte-min* (- (expt 2 7)))
(defparameter *byte-max* (- (expt 2 7) 1))
(defparameter *word-min* (- (expt 2 15)))
(defparameter *word-max* (- (expt 2 15) 1))
(defparameter *dword-min* (- (expt 2 31)))
(defparameter *dword-max* (- (expt 2 31) 1))
(defparameter *qword-min* (- (expt 2 63)))
(defparameter *qword-max* (- (expt 2 63) 1))

(defun byte-as-byte-list (byte)
  (list byte))

(defun dword-as-byte-list (dword)
  (let ((res nil))
    (dotimes (i 4)
      (push (ldb (byte 8 (* i 8)) dword) res))
    res))

(defun qword-as-byte-list (qword)
  (let ((res nil))
    (dotimes (i 8)
      (push (ldb (byte 8 (* i 8)) qword) res))
    res))

(defun immediate-as-byte-list (immediate template)
  (let ((bits (immediate-bits template))
	(signed-immediate (make-signed-immediate immediate template)))
    (let ((res nil))
      (dotimes (i (/ bits 8))
	(push (ldb (byte 8 (* i 8)) signed-immediate) res))
      res)))

(defun make-signed-byte (number)
  (if (minusp number)
      (- (- (expt 2 7) 1) (- number))
      number))

(defun make-signed-dword (number)
  (if (minusp number)
      (- (- (expt 2 31) 1) (- number))
      number))

(defun make-signed-qword (number)
  (if (minusp number)
      (- (- (expt 2 63) 1) (- number))
      number))

(defun make-signed-immediate (number template)
  (if (minusp number)
      (- (- (expt 2 (immediate-bits template)) 1) (- number))
      number))

(defun little-endian-64bit (num)
  (let ((code nil))
    (dotimes (i 8)
      (push (ldb (byte 8 (* i 8)) num) code))
    (reverse code)))

(defun filter (list what)
  (let ((res nil))
    (dolist (l list)
      (when (not (equal l what))
	(push l res)))
    (reverse res)))

(defun hex-opcodes (instructions)
  (mapcar (lambda (x) (format nil "#x~2,'0x" x))
	  instructions))

(defun signed-number-type (number)
  (cond ((and (>= number *byte-min*)
	      (<=  number *byte-max*))
	 'byte)
	((and (>= number *word-min*)
	      (<= number *word-max*))
	 'word)
	((and (>= number *dword-min*)
	      (<= number *dword-max*))
	 'dword)
	((and (>= number *qword-min*)
	      (<= number *qword-max*))
	 'qword)
	(t 'too-large)))

(defun opcode-d-bit (opcode)
  (logbitp 1 opcode))

(defun print-instruction (instruction)
  (let (bytes)
    (dolist (byte instruction)
      (when byte (push byte bytes)))
    (reverse (mapcar (lambda (b) (format nil "#x~2,'0x" b)) bytes))))


(defun make-byte-object ()
  (make-array 8 :initial-element nil))

(defun set-byte-index (byte-object index bit)
  (setf (aref byte-object index) bit))

(defun set-in-byte (byte-object start size what)
  (dotimes (i size)
    (print (list (+ i start) (- size i)))
    (setf (aref byte-object (+ i start)) (logbitp (- size i 1) what))))

